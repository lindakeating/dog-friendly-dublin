'use strict';

/**
 * @ngdoc overview
 * @name myGoogleApp
 * @description
 * # myGoogleApp
 *
 * Main module of the application.
 */
angular
  .module('myGoogleApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'LocalStorageModule',
    'uiGmapgoogle-maps'
  ])
  .config(function(uiGmapGoogleMapApiProvider){
    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyDLgY27xbkbkDYYE7cYkpbO1yE_m0-kaSM',
      v: '3.17',
      libraries: 'weather,geometry,visualization'
    });
  })
  .config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('ls');
  }])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      }).when('/detail/:id', {
        templateUrl: 'views/detail.html',
        controller:'detailCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
