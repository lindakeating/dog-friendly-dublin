'use strict';

/**
 * @ngdoc function
 * @name myGoogleApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the myGoogleApp
 */
angular.module('myGoogleApp')
  .controller('AboutCtrl', function ($scope, uiGmapGoogleMapApi, uiGmapIsReady, $http) {

    $scope.pageClass = 'page-about';

    $scope.markers = [];
    $http.get("json/locations.json").success(function(response) {
      $scope.locations = response.dogFriendly;
      jQuery.each($scope.locations, function(i, val){
        console.log(i);
        console.log(val.location);
        var marker = {};
        marker.name = val.name;
        marker.id = i;
        marker.coords = val.location;
        marker.icon = '/images/pawIcon.png';
        marker.onClick = function(){
          $scope.windowOptions.visible = !$scope.windowOptions.visible;
        }.bind(this);
        $scope.markers.push(marker);

      });

    });

    $scope.windowOptions = {
      visible: false
    };

    $scope.onClick = function() {
      $scope.windowOptions.visible = !$scope.windowOptions.visible;
    };

    $scope.closeClick = function() {
      $scope.windowOptions.visible = false;
    };

    $scope.title = "Window Title!";


    uiGmapGoogleMapApi.then(function(maps){
        $scope.googleMap = {};
        $scope.map = { center: { latitude: 53.347117, longitude: -6.280285 }, zoom: 14 };

    });

    uiGmapIsReady.promise()
      .then(function(instances){
        var maps = instances[0].map;
        $scope.myOnceOnlyFunction(maps);
      });

    $scope.myOnceOnlyFunction = function (maps) {
      var center = maps.getCenter();
      var lat = center.lat();
      var lng = center.lng();
    };


  });
