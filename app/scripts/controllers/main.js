'use strict';

/**
 * @ngdoc function
 * @name myGoogleApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the myGoogleApp
 */

angular.module('myGoogleApp')
  .controller('MainCtrl', function ($scope, localStorageService) {

    $scope.pageClass = 'page-home';

   /* $scope.todos = ['Item 1', 'Item 2', 'Item 3', 'Item 4'];*/
   var todosInStore = localStorageService.get('todos');

    $scope.todos = todosInStore || [];

    $scope.$watch('todos', function(){
      localStorageService.set('todos', $scope.todos);
    }, true);

    $scope.addTodo = function(){
      $scope.todos.push($scope.todo);
      $scope.todo = '';
    };

    $scope.removeTodo = function(index){
      $scope.todos.splice(index, 1);
    };

  });
