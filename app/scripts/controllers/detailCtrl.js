/**
 * Created by lindakeating on 20/07/15.
 */

angular.module('myGoogleApp')
  .controller('detailCtrl', function ($scope, $routeParams, $http) {

    $scope.pageClass = 'page-detail';

     $scope.pubId = $routeParams.id;

    $http.get('locations.json').success(function(data){
      $.each(data.dogFriendly, function(i, v){
        if(i == parseInt($routeParams.id)){
          $scope.detail = v;
        }
      });
    });
  });
